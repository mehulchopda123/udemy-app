import axios from 'axios';

export default axios.create({
    baseURL:'https://api.unsplash.com',
    headers:{
        Authorization: 'Client-ID 809a637a2263b01ab93181f480f4ce14389de4c7b437638a03ff1ce9962dcd3a'
      }
});